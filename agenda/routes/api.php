<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('get', function (Request $request) {
    return response()->json(["prueba" => "probando"]);
});

Route::get('get', function (Request $request) {
    // return response()->json(["prueba" => "probando"]);
    return response()->json(["prueba" => $request]);
    // return $request->user();
});
// Route::post('post', function (Request $request) {
//     return response()->json(["prueba" => "probando"]);
// });

Route::middleware('auth:api')->post('/post', function (Request $request) {
    // return $request->user();
    // return response()->json(["res" => "asd"]);
});

Route::middleware('auth:api')->get('/token', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->post('/token', function () {
    return response()->json(["token" => csrf_token()]);
});
