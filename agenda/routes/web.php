<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('mail/contrasena');
});
Route::get('/pruebas', 'InscripcionController@Prueba');

Route::get('/get', function () {
    // return 
    // return response()->json(["prueba" => "probando"]);
});

Route::post('/post', function (Request $request) {
    return response()->json(["res" => csrf_token()]);
});

Route::get('/token', function () {
    return response()->json(["token" => csrf_token()]);
});

Route::post('/token', 'UsuarioController@prueba');
Route::post('/token', 'UsuarioController@prueba');


Route::post('/login', 'UsuarioController@Login');
Route::post('/sesion', 'UsuarioController@Sesion');
Route::post('/registrar', 'UsuarioController@Registrar');
Route::post('/registro', 'UsuarioController@Registro');
Route::post('/contrasena1', 'UsuarioController@Contrasena1');
Route::post('/contrasena2', 'UsuarioController@Contrasena2');
Route::post('/usuario/eliminar', 'UsuarioController@Eliminar');
Route::post('/usuario/guardar', 'UsuarioController@Guardar');
Route::post('/usuarios/buscar', 'UsuarioController@Buscar');

Route::post('/usuarios', 'UsuarioController@Usuarios');
Route::get('/usuarios', 'UsuarioController@Todos');

Route::post('/escuela/guardar', 'EscuelaController@Guardar');
Route::post('/escuela/guardar/foto', 'EscuelaController@GuardarFoto');
Route::post('/escuela/eliminar', 'EscuelaController@Eliminar');
Route::post('/escuelas', 'EscuelaController@Filtrar');
Route::get('/escuelas', 'EscuelaController@Todas');

Route::post('/materia/guardar', 'MateriaController@Guardar');
Route::post('/materia/eliminar', 'MateriaController@Eliminar');
Route::post('/materias', 'MateriaController@Filtrar');
Route::get('/materias', 'MateriaController@Todas');

Route::post('/grupo/guardar', 'GrupoController@Guardar');
Route::post('/grupo/eliminar', 'GrupoController@Eliminar');
Route::post('/grupos', 'GrupoController@Filtrar');
Route::get('/grupos', 'GrupoController@Todas');

Route::post('/horario/guardar', 'HorarioController@Guardar');
Route::post('/horario/eliminar', 'HorarioController@Eliminar');
Route::post('/horarios', 'HorarioController@Filtrar');
Route::get('/horarios', 'HorarioController@Todas');

Route::post('/inscripcion/guardar', 'InscripcionController@Guardar');
Route::post('/inscripcion/inscribirse', 'InscripcionController@Inscribirse');
Route::post('/inscripcion/eliminar', 'InscripcionController@Eliminar');
Route::post('/inscripciones', 'InscripcionController@Filtrar');
Route::get('/inscripciones', 'InscripcionController@Todas');

Route::post('/materia/guardar', 'MateriaController@Guardar');
Route::post('/materia/eliminar', 'MateriaController@Eliminar');
Route::post('/materia', 'MateriaController@Filtrar');
Route::get('/materia', 'MateriaController@Todas');
