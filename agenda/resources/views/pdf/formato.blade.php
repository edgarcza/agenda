<!doctype html>
<html lang="es" style="background-color: #b63a63;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <title>Agendas EE</title>
        <style>
            .pie {
                text-align: center;
            }
            .pie img {
                margin: 15px;
                width: 200px;
            }
        </style>

    </head>
    <body style="background-color: #b63a63; padding: 20px;">
        <div class="hoja" style="background-color: white; min-height: 100vh; padding:10px 14px;">
            <h1 style="color: #b63a63; text-align: center;">Agenda</h1>
            <hr/>
            @yield('content')
        </div>

        <div class="pie">
            <img src="http://agendas.estudiantesembajadores.com/agenda/public/img/ee_logo.png" alt="Logo Estudiantes Embajadores">
            <img src="http://agendas.estudiantesembajadores.com/agenda/public/img/senl_logo.png" alt="Logo SENL">
        </div>
    </body>
</html>