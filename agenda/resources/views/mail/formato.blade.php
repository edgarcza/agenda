<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <title>Agendas EE</title>

    </head>
    <style>
        h1 {
            color: #b63a63;
            text-align: center;
        }
        .contenido {
            background-color: rgb(245, 245, 245);
            border-radius: 5px;
            padding: 7px 10px;
            margin: 20px auto;
        }
        table tr, table td, table {
            border-color: #b63a63;
        }
    </style>
    <body>
        <!-- <div class="contenido container">
            <div class="row">
                <div class="col-12">
                    <h1>Agenda</h1>
                    <hr>
                    @yield('content')
                </div>
            </div>
        </div> -->

        <table style="width: 100%;" cellspacing="0">
            <tr style="height: 50px;">
                <td colspan="3" style="background-color: #b63a63;"></td>
            </tr>
            <tr>
                <td style="background-color: #b63a63; width: 13vw;"></td>
                <td style="background-color: #fff; padding: 20px;">
                    <h1 style="color: #b63a63; text-align: center;">Agenda</h1>
                    <hr>
                    @yield('content')
                </td>                
                <td style="background-color: #b63a63; width: 13vw;"></td>
            </tr>
            <tr style="height: 50px;">
                <td colspan="3" style="background-color: #b63a63;"></td>
            </tr>
        </table>
    </body>
</html>