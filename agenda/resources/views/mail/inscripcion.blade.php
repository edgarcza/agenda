@extends('mail.formato')

@section('content')
    <h3 style="text-align: center;">Inscripción a examen</h3>

    <div style="text-align: center; padding: 20px;">
        <img style="width: 50%;" src="http://agendas.estudiantesembajadores.com/agenda/public/archivos/escuelas/{{$Inscripcion->id_escuela}}.{{$Inscripcion->foto_ext}}">
    </div>

    <p><b>Estudiante:</b> {{$Inscripcion->nombres}} {{$Inscripcion->apellidos}}</p>
    <p><b>Sede:</b> {{$Inscripcion->escuela}}</p>
    <p><b>Dirección:</b> {{$Inscripcion->direccion}}</p>
    <p><b>Materia:</b> {{$Inscripcion->materia}}</p>
    <p><b>Grupo:</b> {{$Inscripcion->gdescripcion}}</p>
    <p><b>Horario de examen:</b> {{$Inscripcion->horario}}</p>
@endsection