@extends('mail.formato')

@section('content')
    <h3 style="text-align: center;">Cambio de contraseña</h3>
    
    <p>Click en el siguiente botón para cambiar la contraseña</p>

    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-radius: 2px;" bgcolor="#b63a63">
                            <a href="http://agenda.estudiantesembajadores.com/Contrasena?c={{$Usuario->password_cod}}" target="_blank" 
                                style="padding: 8px 12px; border: 1px solid #b63a63;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block; cursor: pointer;">
                                Cambiar contraseña             
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection