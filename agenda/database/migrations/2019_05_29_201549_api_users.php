<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('api_users', function($table)
        {
            $table->string('api_token', 80)
                                ->unique()
                                ->nullable()
                                ->default(null);
        });
        // Schema::table('api_users', function ($table) {
        //     $table->string('api_token', 80)->after('password')
        //                         ->unique()
        //                         ->nullable()
        //                         ->default(null);
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
