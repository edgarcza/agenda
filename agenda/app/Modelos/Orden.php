<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = 'ordenes';
    protected $guarded = ['id'];

    public function tabla() {
        return $this->table;
    }
    
}
