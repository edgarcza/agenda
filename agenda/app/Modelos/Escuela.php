<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    protected $table = 'escuelas';
    protected $guarded = ['id'];
    
}
