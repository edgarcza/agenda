<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = 'grupos';
    protected $guarded = ['id'];
    
}
