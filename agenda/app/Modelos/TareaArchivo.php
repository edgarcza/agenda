<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class TareaArchivo extends Model
{
    protected $table = 'tareas_archivos';
    protected $guarded = ['id'];
    
}
