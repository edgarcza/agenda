<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    protected $fillable = ['nombres', 'apellidos', 'email', 'password'];
    // protected $guarded = ['password'];
}
