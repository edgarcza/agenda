<?php

namespace App\Modelos;

// use Illuminate\Database\Eloquent\Model;

class Upload
{
    public $Base64_Entero;
    public $Base64;
    public $Extension;
    public $Nombre;
    public $Descripcion;
    public $Carpeta;
    public $Archivo;
    public $Datos;

    function __construct($datos) {
        // $this->Modelo = 'App\Modelos\Cuenta';
        // $this->Datos = $datos['Datos'];
        $this->Base64_Entero = $datos['Base64'];
        $this->Base64 = explode(',', $this->Base64_Entero)[1];

        $this->Archivo = $datos['Datos']['Archivo'];
        $this->Nombre = isset($datos['Datos']['Nombre']) ? $datos['Datos']['Nombre']: '';
        $this->Descripcion = isset($datos['Datos']['Descripcion']) ? $datos['Datos']['Descripcion']: '';

        $this->Carpeta = $datos['Carpeta'];
        $this->Datos = $datos['Datos'];

        $this->Extension = explode('.', $this->Archivo);
        $this->Extension = $this->Extension[count($this->Extension) - 1];   
    }

    function Guardar($nombre) {
        // if(!empty($ArchivoGuardado)) {
        $Dir = 'archivos/' . $this->Carpeta;
        if(!is_dir($Dir)) mkdir($Dir);
        $Ruta = $Dir . '/' . $nombre . '.' . $this->Extension;	
        if(file_put_contents($Ruta, base64_decode($this->Base64)))
            return true;
        return false;
    }
}
