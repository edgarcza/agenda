<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Transcript extends Model
{
    protected $table = 'transcript';
    protected $guarded = ['id'];
    
}
