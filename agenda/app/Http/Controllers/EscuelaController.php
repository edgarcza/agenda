<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Escuela as Modelo;

class EscuelaController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Escuela';
    }

    function GuardarFoto(Request $request) {
        $Guardar = $request->all()['datos']['Datos'];
        $Archivo = $request->all()['datos']['Archivo'];
        $Guardado = null;

        if(isset($Archivo['Base64']) && !empty($Archivo['Base64'])) {
            $Base64_Entero = $Archivo['Base64'];
            $Base64 = explode(',', $Base64_Entero)[1];
        }

        $Extension = explode('.', $Archivo['Nombre']);
        $Extension = $Extension[count($Extension) - 1];   

        $Guardar['foto_ext'] = $Extension;

        if(isset($Guardar['id'])) {
            $Guardado = $this->Modelo::find($Guardar['id']);
            $Guardado->fill($Guardar);
            $Guardado->save();
        }
        else {
            $Guardado = $this->Modelo::create($Guardar);
        }

        if(isset($Archivo['Base64']) && !empty($Archivo['Base64'])) {
            $Dir = 'archivos/escuelas';
            if(!is_dir($Dir)) mkdir($Dir);
            $Ruta = $Dir . '/' . $Guardado['id'] . '.' . $Extension;	
            if(file_put_contents($Ruta, base64_decode($Base64)))
                return response()->json(['proceso' => true, 'datos' => $Guardado]);
        }
        else
            return response()->json(['proceso' => true, 'datos' => $Guardado]);
            
        return response()->json(['proceso' => false, 'datos' => $Guardado]);
    }
}
