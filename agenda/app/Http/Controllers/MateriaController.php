<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Materia as Modelo;

class MateriaController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Materia';
    }
}
