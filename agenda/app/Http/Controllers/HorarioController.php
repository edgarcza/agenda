<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Horario as Modelo;

class HorarioController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Horario';
    }
}
