<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Inscripcion as Modelo;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\Inscripcion as InscripcionMail;

class InscripcionController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Inscripcion';
        $this->Joins = [
            ["grupos", "inscripciones.id_grupo", "grupos.id"],
            ["usuarios", "inscripciones.id_usuario", "usuarios.id"],
            ["materias", "grupos.id_materia", "materias.id"],
            ["escuelas", "grupos.id_escuela", "escuelas.id"],
            ["horarios", "grupos.id_horario", "horarios.id"]
        ];
    }
    
    public function Prueba() {
        $Query = Modelo::query();
        foreach ($this->Joins as $Join) {
            $Query->join($Join[0], $Join[1], $Join[2]);
        }
        $Query = $Query->where('inscripciones.id', '=', 29);
        $Query = $Query->select('*', 'inscripciones.id AS id_principal');
        $Inscripcion = $Query->first();
        return view('pdf/inscripcion', compact('Inscripcion'));
    }

    public function Inscribirse(Request $request) {
        $Datos = $request->all()['datos'];
        $Guardado = null;

        // $ID = 14;
            
        // $Query = Modelo::query();
        // foreach ($this->Joins as $Join) {
        //     $Query->join($Join[0], $Join[1], $Join[2]);
        // }
        // $Query = $Query->where('inscripciones.id', '=', $ID);
        // $Query = $Query->select('*');
        // $Inscripcion = $Query->first();
        // $PDF = PDF::loadView('pdf.inscripcion', compact('Inscripcion'));
        // $PDF->save('pdf/'.$ID.'.pdf'); 
        // // Mail::to($Inscripcion['email'])->send(new InscripcionMail($Inscripcion));
        // return response()->json(['proceso' => false, 'datos' => $Query, 'error' => 2]);
        
        $Inscrito = Modelo::where('id_usuario', '=', $Datos['id_usuario'])->where('id_grupo', '=', $Datos['id_grupo'])->first();
        if(!empty($Inscrito))
            return response()->json(['proceso' => false, 'datos' => $Inscrito, 'error' => 1]);

        $DiaIns = date('Y-m-d', strtotime($Datos['dia']));
        $TotalGrupo = Modelo::where('id_grupo', '=', $Datos['id_grupo'])
                        ->where('dia', '=', $DiaIns)
                        ->count();
        if($TotalGrupo >= 15) 
            return response()->json(['proceso' => false, 'datos' => $Inscrito, 'error' => 2]);
            
        $Guardado = $this->Modelo::create($Datos);        

        if(!empty($Guardado)) {
            
            $Query = Modelo::query();
            foreach ($this->Joins as $Join) {
                $Query->join($Join[0], $Join[1], $Join[2]);
            }
            $Query = $Query->where('inscripciones.id', '=', $Guardado['id']);
            $Query = $Query->select('*', 'inscripciones.id AS id_principal');
            $Inscripcion = $Query->first();
            $PDF = PDF::loadView('pdf.inscripcion', compact('Inscripcion'));
            $PDF->save('pdf/'.$Guardado['id'].'.pdf'); 
            Mail::to($Inscripcion['email'])->send(new InscripcionMail($Inscripcion));

            return response()->json(['proceso' => true, 'datos' => $Guardado]);
        }
        return response()->json(['proceso' => false, 'datos' => $Guardar, 'error' => 2]);
    }
}
