<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Modelos\Usuario;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contrasena as ContrasenaMail;
use Auth;

class UsuarioController extends Controller
{
    public function Login(Request $request) {
        $Usuario = $request->all()['datos'];
        if(Auth::attempt($Usuario, true)) {
            $Usuario = Auth::user();
            return response()->json(['proceso' => true, 'usuario' => Auth::user()]);
        }
        else {
            return response()->json(['proceso' => false, 'usuario' => $Usuario]);
        }
    }

    public function Sesion(Request $request) {
        $Usuario = Usuario::where('remember_token', $request->all()['datos']['remember_token'])->first();
        if($Usuario)
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    //
    public function Registrar(Request $request) {
        $Usuario = $request->all()['datos'];

        $Creado = Usuario::create([
            'nombres' => $Usuario['nombres'],
            'apellidos' => $Usuario['apellidos'],
            'email' => $Usuario['email'],
            'password' => Hash::make($Usuario['password']),
        ]);

        if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario, 'error' => 2]);
    }

    public function Registro(Request $request) {
        $Usuario = $request->all()['datos'];

        $Existente = Usuario::where('email', $Usuario['email'])->first();
        if(!empty($Existente))
            return response()->json(['proceso' => false, 'usuario' => $Existente, 'error' => 1]);

        $Tipo = (isset($Usuario['tipo'])) ? $Usuario['tipo'] : 2;

        $Creado = Usuario::create([
            'nombres' => $Usuario['nombres'],
            'apellidos' => $Usuario['apellidos'],
            'email' => $Usuario['email'],
            'tipo' => $Tipo,
            'password' => Hash::make($Usuario['password']),
        ]);

        if(!empty($Creado))
            return response()->json(['proceso' => true, 'usuario' => $Creado]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    public function Contrasena1(Request $request) {
        $Correo = $request->all()['datos'];

        $Usuario = Usuario::where("email", "=", $Correo)->first();

        if(!empty($Usuario)) {
            $Usuario->password_cod = Hash::make($Correo);
            $Usuario->password_ts = time(); 
            $Usuario->save();

            Mail::to($Correo)->send(new ContrasenaMail($Usuario));
            return response()->json(['proceso' => true, 'datos' => $Usuario]);
        }
        return response()->json(['proceso' => false, 'datos' => $Usuario]);
    }

    public function Contrasena2(Request $request) {
        $Datos = $request->all()['datos'];

        if($Datos['password'] != $Datos['password2']) {
            return response()->json(['proceso' => false, 'datos' => $Datos, 'error' => 1]);
        }

        $Usuario = Usuario::where("password_cod", "=", $Datos['cod'])->first();

        if(!empty($Usuario)) {
            $Usuario->password_cod = '';
            $Usuario->password = Hash::make($Datos['password']); 
            $Usuario->save();

            // Mail::to($Correo)->send(new ContrasenaMail($Usuario));
            return response()->json(['proceso' => true, 'datos' => $Usuario]);
        }
        return response()->json(['proceso' => false, 'datos' => $Usuario, 'error' => 2]);
    }

    public function Guardar(Request $request) {
        $Datos = $request->all()['datos'];

        // $Usuario = Usuario::find($Datos['id']);
        // $Usuario
        // $Usuario->email = $Datos['email'];
        // $Usuario['password'] = Hash::make($Datos['password']);
        // $Usuario->save();
        // $Usuario;

        $Usuario = Usuario::find($Datos['id']);
        // $Usuario = new Usuario($Datos);
        $Usuario->fill([
            'email' => $Datos['email'],
            'nombres' => $Datos['nombres'],
            'tipo' => $Datos['tipo'],
            'apellidos' => $Datos['apellidos'],
            ]
        );
        $Usuario->tipo = $Datos['tipo'];
        if(isset($Datos['password']) && !empty($Datos['password']))
            $Usuario->password = Hash::make($Datos['password']);
        $Usuario->save();

        // if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario, 'datis'=> $Datos]);
        // return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->all()['datos'];

        $Usuario = Usuario::where('id', $ID)->delete();
        // $Usuario;

        // if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        // return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    
    public function Usuarios(Request $request) {
        $Filtros = $request->all()['datos']['Filtros'];
        $Paginador = $request->all()['datos']['Paginador'];
        
        $Query = Usuario::query();
        foreach ($Filtros as $Filtro) {
            $Query = $Query->where($Filtro['Campo'], 'like', '%'.$Filtro['Valor'].'%');
        }
        $DatosUL = $Query->get()->count();
        $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
        $Query = $Query->limit($Paginador['Cantidad']);
        $Datos = $Query->get();



        return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos']['Paginador']]);
    }

    public function Todos() {        
        $Datos = Usuario::all();

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }

    function Buscar(Request $request) {
        $Filtro = $request->all()['datos']['Filtro'];
        $Query = Usuario::query();
        $Query = $Query->where('nombres', 'like', '%'.$Filtro.'%');
        $Query = $Query->orWhere('apellidos', 'like', '%'.$Filtro.'%');
        $Query = $Query->orWhere('email', 'like', '%'.$Filtro.'%');
        $Datos = $Query->get();

        return response()->json(['proceso' => true, 'datos' => $Datos, 'req' => $request->all()['datos']]);
    }
}
