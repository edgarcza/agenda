<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Transcript as Modelo;

class TranscriptController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Transcript';
    }
}
