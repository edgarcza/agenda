<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Orden as Modelo;

class OrdenController extends BaseController
{

    function __construct() {
        $this->Modelo = 'App\Modelos\Orden';
        $this->Joins = [
            ["cursos", "ordenes.id_curso", "cursos.id"],
            ["alumnos", "ordenes.id_alumno", "alumnos.id"],
            ["materias", "cursos.id_materia", "materias.id"],
            ["instituciones", "ordenes.id_institucion", "instituciones.id"],
            ["tipos_curso", "cursos.id_tipo_curso", "tipos_curso.id"],
            ["ordenes_estatus", "ordenes_estatus.id", "ordenes.id_estatus"]
        ];
    }
}
