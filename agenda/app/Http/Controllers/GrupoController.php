<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Grupo as Modelo;

class GrupoController extends BaseController
{

    function __construct() {
        $this->Modelo = 'App\Modelos\Grupo';
        $this->Joins = [
            ["materias", "grupos.id_materia", "materias.id"],
            ["escuelas", "grupos.id_escuela", "escuelas.id"],
            ["horarios", "grupos.id_horario", "horarios.id"]
        ];
    }
}
