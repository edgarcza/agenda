<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Inscripcion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $Inscripcion;

    public function __construct($ins)
    {
        $this->Inscripcion = $ins;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->from('agenda@estudiantesembajadores.com')
        //             ->view('mail.inscripcion');
        return $this->view('mail.inscripcion')
                    ->attach(public_path('pdf').'/'.$this->Inscripcion['id_principal'].'.pdf', [
                            'as' => 'Inscripción.pdf',
                            'mime' => 'application/pdf',
                    ]);
    }
}
