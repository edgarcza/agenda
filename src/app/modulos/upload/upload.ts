/**
 * Ruta: link para enviar datos
 * Carpeta: carpeta para guardarlo
 * Multiple: true para permitir 2 o +
 * Nombres: true para poder poner un nombre
 * Descripcion: true para poder poner descripcion
 * Datos: los datos extra para el servidor
 */
export interface UploadDatos {
    /** link para enviar datos */
    Ruta: string,
    /** carpeta para guardarlo */
    Carpeta: string,
    /** true para permitir 2 o + */
    Multiple: boolean,
    /** true para poder poner un nombre */
    Nombres: boolean,
    /** true para poder poner descripcion */
    Descripcion: boolean,
    /** los datos extra para el servidor */
    Datos: {}
}