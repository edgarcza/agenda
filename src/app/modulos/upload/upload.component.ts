import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  
  @ViewChild('Archivo') Archivo;
  public ListaArchivos = [];
  public Archivos: Set<File> = new Set()


  constructor(
    public DRef: MatDialogRef<UploadComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    public Http: HttpService,
    private Alertas: AlertasService
  ) { 
    
  }

  ngOnInit() {
  }

  AgregarArchivos() {    
    this.Archivo.nativeElement.click();
  }

  ArchivoAgregado() {
    const files: { [key: string]: File } = this.Archivo.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.Archivos.add(files[key]);
      }
    }
    // console.log(this.Archivos);
    this.ListaArchivos = [];
    this.Archivos.forEach((val) => {
      this.ListaArchivos.push(val);
    })

    console.log(this.Archivos);
    // this.ListaArchivos = this.Archivo.nativeElement.files;
    // console.log(this.Archivo.nativeElement.files);
  }

  SubirArchivos() {
    // const Datos: FormData = new FormData();
    // for(let f in this.ListaArchivos) {
    //   Datos.append('file', this.ListaArchivos[f], this.ListaArchivos[f].name);
    // }
    
    // let headers = new HttpHeaders();
    // headers = headers.append('Accept', 'application/json');
    // headers = headers.append('Content-Type', 'multipart/form-data');

    // headers.append('Authorization','Bearer ' + localStorage.token );

    let i = 0;
    this.Archivos.forEach(archivo => {
      // console.log(archivo)
      // const Datos: FormData = new FormData();
      // Datos.append('file', archivo, archivo.name);
      let Reader = new FileReader();
      Reader.readAsDataURL(archivo);
      Reader.onload = () => {
        console.log(this.ListaArchivos[i]);
        // let Datos = {
          // 
        // }
        let Mandar: any = {
          Base64: Reader.result,
          Carpeta: this.DialogDatos.Carpeta,
          Datos: {
            Archivo: archivo.name, 
            Nombre: this.ListaArchivos[i].nombre, 
            Descripcion: this.ListaArchivos[i].descripcion
          }
        };
        Object.assign(Mandar.Datos, this.DialogDatos.Datos);
        this.Http.post(this.DialogDatos.Ruta, Mandar).subscribe((res: any) => {
          console.log(res);
          if(i == this.Archivos.size) {
            // if(res.proceso) {
              this.Cerrar(1);
            // }
            // else {
            //   this.Cerrar(2);
            // }
          }
        })
        i++;
          
      }

      // this.Http.Http.post(this.Http.Url + this.DialogDatos.Ruta, Datos,  { headers: { 'Content-Type': 'multipart/form-data' } })
      // .subscribe((res) => {
      //   console.log(res);
      // })
    })
  }

  EliminarArchivo(i) {
    // console.log(this.ListaArchivos[i]);
    let NuevaLista = [], c = 0;
    for(let ia in this.ListaArchivos) {
      if(ia != i) {
        NuevaLista[c] = this.ListaArchivos[ia];
        c++;
      }
    }
    this.ListaArchivos = NuevaLista;
    // this.ListaArchivos.splice(i, 1);
  }

  Cerrar(arg = 0) {
    this.DRef.close(arg);
  }

}
