import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  @Output() add = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  ADD() {
    this.add.emit(1);
  }

}
