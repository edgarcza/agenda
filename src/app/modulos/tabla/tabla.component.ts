import { Component, OnInit, Input, Output, ElementRef, HostListener, EventEmitter, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { VentanaService } from '../../servicios/ventana.service'
import { Observable } from 'rxjs';
import { TablaConfig } from "./tabla"
import { ColumnasComponent } from "./columnas.component"
/* import * as roundTo from 'round-to' */

@Component({
  selector: 'tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss']
})
export class TablaComponent implements OnInit {

  @Input("datos") Datos;
  @Input("filtros") Filtros = true;
  @Input("paginador") Paginador = true;
  @Input("simple") Simple = false;
  @Input("configuracion") TablaConfig: TablaConfig;
  // @Input() cambioDatos: Observable<void>;
  @Output() cambioSeleccion = new EventEmitter();
  // @Output() accion = new EventEmitter();
  @Output() tabla = new EventEmitter();
  @Output() evento = new EventEmitter();
  @Output() accion = new EventEmitter();

  // Columnas
  // Acciones

  // @Input("datosFuente") Datos;
  // @Input("datosFuente") Columnas;
  // @Input("datosFuente") ColumnasIds;

  // public TablaConfig: TablaConfig = {Filtros: [], Paginador: {Cantidad: 10, Pagina: 1, Total: 0}};
  public AccionesVer: boolean[] = [];
  public SeleccionFilas: boolean[] = [];
  public Totales: number[] = [];
  public SeleccionTodas = false;
  public FilasSeleccionadas = [];
  public FiltroSelect = [];
  public Construir = false;

  constructor(
    private ERef: ElementRef,
    private Ventana: VentanaService
  ) { }

  ngOnInit() 
  {
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("change");
    if(this.Datos) {
      for(var C in this.Datos.Campos) {
        if(this.Datos.Campos[C].Total == true) { 
          this.Totales[this.Datos.Campos[C].Id] = 0;
          for(var CF in this.Datos.Filas) {
            // this.Totales[this.Datos.Campos[C].Id] += parseInt(this.Datos.Filas[CF][this.Datos.Campos[C].Id]);
            // this.Totales[this.Datos.Campos[C].Id] += roundTo(parseFloat(this.Datos.Filas[CF][this.Datos.Campos[C].Id]), 2);
          }
         // this.Totales[this.Datos.Campos[C].Id] = roundTo(this.Totales[this.Datos.Campos[C].Id], 2);
        }
      }

      this.SeleccionFilas = [];
      for(var F in this.Datos.Filas) {
        this.AccionesVer[this.Datos.Filas[F].id] = false;
        this.SeleccionFilas[F] = false;
      }
      this.Construir = true;
      console.log(this.SeleccionFilas);
    }
  }

  ConfigurarTabla() {
    this.tabla.emit(this.TablaConfig);
  }

  AccionClick(accion, datos) {
    this.accion.emit({Accion: accion, Datos: datos});
  }

  FiltroCambio(caso, valor) {
    if(caso == 'select') {
      for(var i in this.TablaConfig.Filtros) {
        if(this.FiltroSelect.indexOf(this.TablaConfig.Filtros[i].Campo) != -1) {
          this.TablaConfig.Filtros[i].Filtrar = true;
        }
        else {
          this.TablaConfig.Filtros[i].Filtrar = false;
        }
      }
    }
    else if(caso == 'input') {
      this.ConfigurarTabla();
    }
  }

  PaginaCambio(pagina) {
    this.TablaConfig.Paginador.Cantidad = pagina.pageSize;
    this.TablaConfig.Paginador.Pagina = pagina.pageIndex;
    this.TablaConfig.Paginador.Total = pagina.length;
    this.ConfigurarTabla();
  }

  ActualizarDatos() {    
    for(var F in this.Datos.Filas) {
      this.AccionesVer[this.Datos.Filas[F].id] = false;
      this.SeleccionFilas[F] = false;
    }
  }

  VerAcciones(acc) {    
    for(var A in this.AccionesVer) {
      if(this.AccionesVer[A] != this.AccionesVer[acc])
        this.AccionesVer[A] = false;
    }
    this.AccionesVer[acc] = !this.AccionesVer[acc];
  }

  @HostListener('document:click', ['$event'])
  clickout(event)  {
    if(event.target.nodeName !== "LI" && event.target.nodeName !== "I") {
      for(var A in this.AccionesVer) {
          this.AccionesVer[A] = false;
      }
    }
  }

  SeleccionarTodas() {
    this.SeleccionTodas = !this.SeleccionTodas;
    // console.log(this.SeleccionTodas);

    // this.SeleccionFilas[id] = !this.SeleccionFilas[id];
    for(var S in this.SeleccionFilas) {
        // if()
        this.SeleccionFilas[S] = this.SeleccionTodas;
    }
    this.ActualizarFilasSeleccionadas();
  } 
  
  SeleccionarFila(id) {
    // this.SeleccionFilas[id] = !this.SeleccionFilas[id];
    // console.log(this.SeleccionFilas[id]);
    this.ActualizarFilasSeleccionadas();
  }

  ActualizarFilasSeleccionadas() {
    let c = 0;
    this.FilasSeleccionadas = [];
    for(var S in this.SeleccionFilas) {
        if(this.SeleccionFilas[S]) {
          this.FilasSeleccionadas[c] = this.Datos.Filas[S];
          c++;
        }
    }
    console.log(this.FilasSeleccionadas);
    this.cambioSeleccion.emit(this.FilasSeleccionadas);
  }

  Columnas() {
    // this.Ventana.Abrir(ColumnasComponent);
    this.Ventana.Abrir(ColumnasComponent, {Ancho: '400px', Datos: this.Datos}).subscribe(
      (ventana) => {
        console.log(ventana);
      }
    )
  }

  Excel() {
    this.evento.emit("Excel");
  }

}
