import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit {

  constructor(
    public DRef: MatDialogRef<DialogoComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    ) { }

  ngOnInit() {
  }

  Accion(i) {
    this.DRef.close(i + 1);
  }

}
