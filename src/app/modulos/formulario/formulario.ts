import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
export class Formulario
{

    constructor() {

    }

    Campo(placeholder, nombre, tipo, validadores = null, col = 12, opciones = [], predem = null) {
        let campo =  {
            PH: placeholder,
            Nombre: nombre,
            Tipo: tipo,
            Validadores: validadores,
            Col: col,
            Opciones: opciones,
            Default: predem
        };
        return campo;
    }

    CampoMod(controles, campo, valores = {}) {
        for(var i in controles) {
            // console.log(controles[i]);
            if(controles[i].Nombre === campo) {
                Object.assign(controles[i], valores);
                return;
            }
        }
    }

    CampoSelect(controles, campo, opciones = [], valor, nombre) {
        var OPFormato = opciones.map(function(elm) {
            return { Valor: elm[valor], Nombre: elm[nombre]};
        });
         
        this.CampoMod(controles, campo, {Opciones: OPFormato});
    }
}