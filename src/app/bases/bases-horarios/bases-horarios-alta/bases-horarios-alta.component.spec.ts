import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesHorariosAltaComponent } from './bases-horarios-alta.component';

describe('BasesHorariosAltaComponent', () => {
  let component: BasesHorariosAltaComponent;
  let fixture: ComponentFixture<BasesHorariosAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesHorariosAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesHorariosAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
