import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-bases-horarios-alta',
  templateUrl: './bases-horarios-alta.component.html',
  styleUrls: ['./bases-horarios-alta.component.css']
})
export class BasesHorariosAltaComponent implements OnInit {

  BCAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Hora', 'hh', 'text', [Validators.required], 6, [{}]),
    new Formulario().Campo('Minuto', 'mm', 'text', [Validators.required], 6, [{}]),
    new Formulario().Campo('Descripción', 'descripcion', 'textarea', [Validators.required], 12, [{}]),
  ];
  public BCAForm = new FormGroup({});

  constructor(
    public DRef: MatDialogRef<BasesHorariosAltaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    private Http: HttpService,
    private Alertas: AlertasService
  ) { }

  ngOnInit() {
    
  }

  Cerrar(caso = 0) {
    this.DRef.close(caso);
  }

  Submit(caso) {
    let Horario: any = this.BCAForm.value;
    let sHH, sMM;
    sHH = (parseInt(Horario.hh) < 10) ? '0'+Horario.hh : Horario.hh;
    sMM = (parseInt(Horario.mm) < 10) ? '0'+Horario.mm : Horario.mm;
    Horario.horario = `${sHH}:${sMM}`;
    console.log(Horario);
    this.Http.post("horario/guardar", Horario).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Horario guardado", "Cerrar");
        this.Cerrar(caso);
      }
    })
  }

}
