import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesHorariosComponent } from './bases-horarios.component';

describe('BasesHorariosComponent', () => {
  let component: BasesHorariosComponent;
  let fixture: ComponentFixture<BasesHorariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesHorariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesHorariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
