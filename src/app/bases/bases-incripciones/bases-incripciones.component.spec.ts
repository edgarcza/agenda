import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesIncripcionesComponent } from './bases-incripciones.component';

describe('BasesIncripcionesComponent', () => {
  let component: BasesIncripcionesComponent;
  let fixture: ComponentFixture<BasesIncripcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesIncripcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesIncripcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
