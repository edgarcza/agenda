import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-bases-inscripciones-alta',
  templateUrl: './bases-inscripciones-alta.component.html',
  styleUrls: ['./bases-inscripciones-alta.component.css']
})
export class BasesInscripcionesAltaComponent implements OnInit {

  BCAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Calificación', 'calificacion', 'number', [Validators.required], 12),
    // new Formulario().Campo('Escuela', 'id_escuela', 'select', [Validators.required], 12, [{}]),
    // new Formulario().Campo('Materia', 'id_materia', 'select', [Validators.required], 12, [{}]),
    // new Formulario().Campo('Horario', 'id_horario', 'select', [Validators.required], 12, [{}]),
    // new Formulario().Campo('Descripción', 'gdescripcion', 'textarea', [Validators.required], 12, [{}]),
    // new Formulario().Campo('Fecha de examen', 'fecha_examen', 'date', [Validators.required], 12, [{}]),
  ];
  public BCAForm = new FormGroup({});

  constructor(
    public DRef: MatDialogRef<BasesInscripcionesAltaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    private Http: HttpService,
    private Alertas: AlertasService
  ) { }

  ngOnInit() {
    
  }

  Cerrar(caso = 0) {
    this.DRef.close(caso);
  }

  Submit(caso) {
    let Inscripcion: any = {};
    Inscripcion.id = this.DialogDatos.id_principal;
    Inscripcion.calificacion = this.BCAForm.value.calificacion;
    console.log(Inscripcion)
    this.Http.post("inscripcion/guardar", Inscripcion).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Inscripción guardada", "Cerrar");
        this.Cerrar(caso);
      }
    })
  }

}
