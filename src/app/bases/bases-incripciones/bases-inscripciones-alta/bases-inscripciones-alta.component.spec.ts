import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesInscripcionesAltaComponent } from './bases-inscripciones-alta.component';

describe('BasesInscripcionesAltaComponent', () => {
  let component: BasesInscripcionesAltaComponent;
  let fixture: ComponentFixture<BasesInscripcionesAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesInscripcionesAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesInscripcionesAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
