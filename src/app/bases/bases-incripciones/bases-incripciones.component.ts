import { Component, OnInit } from '@angular/core';
import { Tabla } from 'src/app/modulos/tabla/tabla';
import { HttpService } from 'src/app/servicios/http.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { BasesInscripcionesAltaComponent } from './bases-inscripciones-alta/bases-inscripciones-alta.component';

@Component({
  selector: 'app-bases-incripciones',
  templateUrl: './bases-incripciones.component.html',
  styleUrls: ['./bases-incripciones.component.css']
})
export class BasesIncripcionesComponent extends Tabla implements OnInit {

  constructor(HTTP: HttpService, private Ventana: VentanaService, private Alertas: AlertasService) {
    super(HTTP);
  }

  ngOnInit() {
    this.ConfigurarTabla({
      Tabla: {
        Nombre: "inscripciones",
        Campos: [
          // {Id: 'email',     Nombre: 'Usuario', Ocultar: false,    Campo: 'materias.materia',     Total: false},
          { Id: 'nombres', Nombre: 'Nombres', Ocultar: false, Campo: 'usuarios.nombres', Total: false },
          { Id: 'apellidos', Nombre: 'Apellidos', Ocultar: false, Campo: 'usuarios.apellidos', Total: false },
          { Id: 'escuela', Nombre: 'Sede', Ocultar: false, Campo: 'escuelas.escuela', Total: false },
          { Id: 'materia', Nombre: 'Materia', Ocultar: false, Campo: 'materias.materia', Total: false },
          { Id: 'horario', Nombre: 'Hora', Ocultar: false, Campo: 'horarios.horario', Total: false },
          { Id: 'dia', Nombre: 'Fecha', Ocultar: false, Campo: 'inscripciones.dia', Total: false },
          // {Id: 'hh',     Nombre: 'Hora', Ocultar: false,    Campo: 'horarios.hh',     Total: false},
          // {Id: 'mm',     Nombre: 'Minuto', Ocultar: false,    Campo: 'horarios.mm',     Total: false},
          { Id: 'gdescripcion', Nombre: 'Grupo', Ocultar: false, Campo: 'grupos.gdescripcion', Total: false },
          { Id: 'calificacion', Nombre: 'Calificación', Ocultar: false, Campo: 'inscripciones.calificacion', Total: false },
          { Id: 'fecha_examen', Nombre: 'Fecha del examen', Ocultar: false, Campo: 'grupos.fecha_examen ', Total: false },
        ],
        Acciones: [
          {
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          },
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          }
        ]
      }
    });
  }

  Accion(arg) {
    console.log(arg);
    if (arg.Accion == "Eliminar") {
      this.HTTP.post("inscripcion/eliminar", arg.Datos.id_principal).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Inscripción eliminada", "Cerrar");
          this.Tabla();
        }
      })
    }
    else if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(BasesInscripcionesAltaComponent, { Ancho: '40%', Datos: arg.Datos }).subscribe(
        respuesta => {
          console.log(respuesta);
          if (respuesta == 1)
            this.Tabla();
        }
      );
    }
  }

}
