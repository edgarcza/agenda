import { Component, OnInit } from '@angular/core';
import { Tabla } from 'src/app/modulos/tabla/tabla';
import { HttpService } from 'src/app/servicios/http.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { BasesMateriasAltaComponent } from './bases-materias-alta/bases-materias-alta.component';
// import {}

@Component({
  selector: 'app-bases-materias',
  templateUrl: './bases-materias.component.html',
  styleUrls: ['./bases-materias.component.css']
})
export class BasesMateriasComponent extends Tabla implements OnInit {

  constructor(HTTP: HttpService, private Ventana: VentanaService, private Alertas: AlertasService) {
    super(HTTP);
   }

  ngOnInit() {
    this.ConfigurarTabla({
      Tabla: {
        Nombre: "materias",
        Campos: [
          {Id: 'materia',     Nombre: 'Materia', Ocultar: false,    Campo: 'materias.materia',     Total: false},
        ],  
        Acciones: [
          {
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          },
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          }
        ]
      }
    });
  }

  Agregar() {
    this.Ventana.Abrir(BasesMateriasAltaComponent, {Ancho: '40%', Datos: null}).subscribe((respuesta) => {
      if(respuesta == 1) {
        this.Tabla();
      }
      else if(respuesta == 2) {
        this.Tabla();
        this.Agregar();
      }
    })
  }

  Accion(arg) {
    console.log(arg);
    if(arg.Accion == "Eliminar") {
      this.HTTP.post("materia/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Materia eliminada", "Cerrar");
          this.Tabla();
        }
      })
    }
    else if(arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(BasesMateriasAltaComponent, {Ancho: '40%', Datos: arg.Datos}).subscribe(
        respuesta => {
          console.log(respuesta);
          if(respuesta == 1)
            this.Tabla();
        }
      );
    }
  }

}
