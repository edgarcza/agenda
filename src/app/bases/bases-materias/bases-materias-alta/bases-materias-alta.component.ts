import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-bases-materias-alta',
  templateUrl: './bases-materias-alta.component.html',
  styleUrls: ['./bases-materias-alta.component.css']
})
export class BasesMateriasAltaComponent implements OnInit {

  BMAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Nombre de materia', 'materia', 'text', [Validators.required], 12),
  ];
  public BMAForm = new FormGroup({});

  constructor(
    public DRef: MatDialogRef<BasesMateriasAltaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    private Http: HttpService,
    private Alertas: AlertasService
  ) { }

  ngOnInit() {
  }

  Cerrar(caso = 0) {
    this.DRef.close(caso);
  }

  Submit(caso) {
    this.Http.post("materia/guardar", this.BMAForm.value).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Materia guardada", "Cerrar");
        this.Cerrar(caso);
      }
    })
  }

}
