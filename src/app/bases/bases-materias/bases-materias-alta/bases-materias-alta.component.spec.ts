import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesMateriasAltaComponent } from './bases-materias-alta.component';

describe('BasesMateriasAltaComponent', () => {
  let component: BasesMateriasAltaComponent;
  let fixture: ComponentFixture<BasesMateriasAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesMateriasAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesMateriasAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
