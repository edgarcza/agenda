import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesMateriasComponent } from './bases-materias.component';

describe('BasesMateriasComponent', () => {
  let component: BasesMateriasComponent;
  let fixture: ComponentFixture<BasesMateriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesMateriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesMateriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
