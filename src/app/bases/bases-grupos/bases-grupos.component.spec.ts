import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesGruposComponent } from './bases-grupos.component';

describe('BasesGruposComponent', () => {
  let component: BasesGruposComponent;
  let fixture: ComponentFixture<BasesGruposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesGruposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesGruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
