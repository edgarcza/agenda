import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesGruposAltaComponent } from './bases-grupos-alta.component';

describe('BasesGruposAltaComponent', () => {
  let component: BasesGruposAltaComponent;
  let fixture: ComponentFixture<BasesGruposAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesGruposAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesGruposAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
