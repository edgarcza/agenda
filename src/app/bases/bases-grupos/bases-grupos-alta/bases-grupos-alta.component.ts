import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-bases-grupos-alta',
  templateUrl: './bases-grupos-alta.component.html',
  styleUrls: ['./bases-grupos-alta.component.css']
})
export class BasesGruposAltaComponent implements OnInit {

  BCAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Escuela', 'id_escuela', 'select', [Validators.required], 12, [{}]),
    new Formulario().Campo('Materia', 'id_materia', 'select', [Validators.required], 12, [{}]),
    new Formulario().Campo('Horario', 'id_horario', 'select', [Validators.required], 12, [{}]),
    new Formulario().Campo('Grupo', 'gdescripcion', 'textarea', [Validators.required], 12, [{}]),
    new Formulario().Campo('Fecha de examen', 'fecha_examen', 'date', [Validators.required], 12, [{}]),
  ];
  public BCAForm = new FormGroup({});

  constructor(
    public DRef: MatDialogRef<BasesGruposAltaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    private Http: HttpService,
    private Alertas: AlertasService
  ) { }

  ngOnInit() {
    this.Http.get("escuelas").subscribe((res: any) => {
      new Formulario().CampoSelect(this.BCAControles, "id_escuela", res.datos, "id", "escuela");
    });
    
    this.Http.get("materias").subscribe((res: any) => {
      new Formulario().CampoSelect(this.BCAControles, "id_materia", res.datos, "id", "materia");
    });
    
    this.Http.get("horarios").subscribe((res: any) => {
      new Formulario().CampoSelect(this.BCAControles, "id_horario", res.datos, "id", "horario");
      // var OPFormato = res.datos.map(function(elm) {
      //     return { Valor: elm["id"], Nombre: elm[nombre]};
      // });
       
      // this.CampoMod(controles, campo, {Opciones: OPFormato});
    });
  }

  Cerrar(caso = 0) {
    this.DRef.close(caso);
  }

  Submit(caso) {
    let Grupo: any = this.BCAForm.value;
    Grupo.id = (this.DialogDatos) ? this.DialogDatos.id_principal : null;
    let Fecha = new Date(Grupo.fecha_examen);
    Grupo.fecha_examen = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;
    this.Http.post("grupo/guardar", Grupo).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Grupo guardado", "Cerrar");
        this.Cerrar(caso);
      }
    })
  }

}
