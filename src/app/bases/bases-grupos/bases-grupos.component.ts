import { Component, OnInit } from '@angular/core';
import { Tabla } from 'src/app/modulos/tabla/tabla';
import { HttpService } from 'src/app/servicios/http.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { BasesGruposAltaComponent } from './bases-grupos-alta/bases-grupos-alta.component';

@Component({
  selector: 'app-bases-grupos',
  templateUrl: './bases-grupos.component.html',
  styleUrls: ['./bases-grupos.component.css']
})
export class BasesGruposComponent extends Tabla implements OnInit {

  constructor(HTTP: HttpService, private Ventana: VentanaService, private Alertas: AlertasService) {
    super(HTTP);
   }

  ngOnInit() {
    this.ConfigurarTabla({
      Tabla: {
        Nombre: "grupos",
        Campos: [
          {Id: 'materia',     Nombre: 'Materia', Ocultar: false,    Campo: 'materias.materia',     Total: false},
          {Id: 'escuela',     Nombre: 'Escuela', Ocultar: false,    Campo: 'escuelas.escuela',     Total: false},
          {Id: 'horario',     Nombre: 'Hora', Ocultar: false,    Campo: 'horarios.horario',     Total: false},
          // {Id: 'hh',     Nombre: 'Hora', Ocultar: false,    Campo: 'horarios.hh',     Total: false},
          // {Id: 'mm',     Nombre: 'Minuto', Ocultar: false,    Campo: 'horarios.mm',     Total: false},
          {Id: 'fecha_examen',     Nombre: 'Fecha de examen', Ocultar: false,    Campo: 'grupos.fecha_examen',     Total: false},
          {Id: 'gdescripcion',     Nombre: 'Descripción', Ocultar: false,    Campo: 'grupos.gdescripcion',     Total: false},
        ],  
        Acciones: [
          {
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          },
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          }
        ]
      }
    });
  }

  Agregar() {
    this.Ventana.Abrir(BasesGruposAltaComponent, {Ancho: '40%', Datos: null}).subscribe((respuesta) => {
      if(respuesta == 1) {
        this.Tabla();
      }
      else if(respuesta == 2) {
        this.Tabla();
        this.Agregar();
      }
    })
  }

  Accion(arg) {
    console.log(arg);
    if(arg.Accion == "Eliminar") {
      this.HTTP.post("grupo/eliminar", arg.Datos.id_principal).subscribe((respuesta: any) => {
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Grupo eliminado", "Cerrar");
          this.Tabla();
        }
      })
    }
    else if(arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(BasesGruposAltaComponent, {Ancho: '40%', Datos: arg.Datos}).subscribe(
        respuesta => {
          console.log(respuesta);
          if(respuesta == 1)
            this.Tabla();
        }
      );
    }
  }

}
