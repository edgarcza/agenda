import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesUsuariosComponent } from './bases-usuarios.component';

describe('BasesUsuariosComponent', () => {
  let component: BasesUsuariosComponent;
  let fixture: ComponentFixture<BasesUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
