import { Component, OnInit } from '@angular/core';
import { Tabla } from 'src/app/modulos/tabla/tabla';
import { HttpService } from 'src/app/servicios/http.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { BasesUsuariosAltaComponent } from './bases-usuarios-alta/bases-usuarios-alta.component';

@Component({
  selector: 'app-bases-usuarios',
  templateUrl: './bases-usuarios.component.html',
  styleUrls: ['./bases-usuarios.component.css']
})
export class BasesUsuariosComponent extends Tabla implements OnInit {

  constructor(HTTP: HttpService, private Ventana: VentanaService, private Alertas: AlertasService) {
    super(HTTP);
   }

  ngOnInit() {
    this.ConfigurarTabla({
      Tabla: {
        Nombre: "usuarios",
        Campos: [
          {Id: 'nombres',     Nombre: 'Nombres', Ocultar: false,    Campo: 'usuarios.nombres',     Total: false},
          {Id: 'apellidos',     Nombre: 'Apellidos', Ocultar: false,    Campo: 'usuarios.apellidos',     Total: false},
          {Id: 'email',     Nombre: 'Correo electrónico', Ocultar: false,    Campo: 'usuarios.email',     Total: false},
          // {Id: 'hh',     Nombre: 'Hora', Ocultar: false,    Campo: 'horarios.hh',     Total: false},
          // {Id: 'mm',     Nombre: 'Minuto', Ocultar: false,    Campo: 'horarios.mm',     Total: false},
          // {Id: 'descripcion',     Nombre: 'Descripción', Ocultar: false,    Campo: 'horarios.descripcion',     Total: false},
        ],  
        Acciones: [
          {
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          },
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          }
        ]
      }
    });
  }

  Agregar() {
    this.Ventana.Abrir(BasesUsuariosAltaComponent, {Ancho: '40%', Datos: null}).subscribe((respuesta) => {
      if(respuesta == 1) {
        this.Tabla();
      }
      else if(respuesta == 2) {
        this.Tabla();
        this.Agregar();
      }
    })
  }

  Accion(arg) {
    console.log(arg);
    if(arg.Accion == "Eliminar") {
      this.HTTP.post("usuario/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Usuario eliminado", "Cerrar");
          this.Tabla();
        }
      })
    }
    else if(arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(BasesUsuariosAltaComponent, {Ancho: '40%', Datos: arg.Datos}).subscribe(
        respuesta => {
          console.log(respuesta);
          if(respuesta == 1)
            this.Tabla();
        }
      );
    }
  }

}
