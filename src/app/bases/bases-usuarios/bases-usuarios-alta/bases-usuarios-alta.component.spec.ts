import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesUsuariosAltaComponent } from './bases-usuarios-alta.component';

describe('BasesUsuariosAltaComponent', () => {
  let component: BasesUsuariosAltaComponent;
  let fixture: ComponentFixture<BasesUsuariosAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesUsuariosAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesUsuariosAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
