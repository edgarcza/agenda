import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-bases-usuarios-alta',
  templateUrl: './bases-usuarios-alta.component.html',
  styleUrls: ['./bases-usuarios-alta.component.css']
})
export class BasesUsuariosAltaComponent implements OnInit {

  BCAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Nombres', 'nombres', 'text', [Validators.required], 12, [{}]),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 12, [{}]),
    new Formulario().Campo('Correo electrónico', 'email', 'text', [Validators.required, Validators.email], 12, [{}]),
    new Formulario().Campo('Tipo de usuario', 'tipo', 'select', [Validators.required], 12, [
      {Nombre: 'Usuario', Valor: 2},
      {Nombre: 'Administrador', Valor: 1},
    ]),
    new Formulario().Campo('Contraseña', 'password', 'password', [], 12, [{}]),
  ];
  public BCAForm = new FormGroup({});

  constructor(
    public DRef: MatDialogRef<BasesUsuariosAltaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    private Http: HttpService,
    private Alertas: AlertasService
  ) { }

  ngOnInit() {
    
  }

  Cerrar(caso = 0) {
    this.DRef.close(caso);
  }

  Submit(caso) {
    let Usuario: any = this.BCAForm.value;
    if(this.DialogDatos) {
      console.log(Usuario)
      this.Http.post("usuario/guardar", Usuario).subscribe((respuesta: any) => {
        console.log(respuesta);
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Usuario guardado", "Cerrar");
          this.Cerrar(caso);
        }
        else {
          // if(respuesta.error == 1) {
          this.Alertas.MSB_Mostrar("Error", "Cerrar");
          // }
        }
      })
    }
    else {
      this.Http.post("registro", Usuario).subscribe((respuesta: any) => {
        console.log(respuesta);
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Usuario guardado", "Cerrar");
          this.Cerrar(caso);
        }
        else {
          if(respuesta.error == 1) {
            this.Alertas.MSB_Mostrar("Correo ya existe", "Cerrar");
          }
        }
      })
    }
  }

}
