import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesEscuelasComponent } from './bases-escuelas.component';

describe('BasesEscuelasComponent', () => {
  let component: BasesEscuelasComponent;
  let fixture: ComponentFixture<BasesEscuelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesEscuelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesEscuelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
