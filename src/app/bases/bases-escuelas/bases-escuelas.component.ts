import { Component, OnInit } from '@angular/core';
import { Tabla } from 'src/app/modulos/tabla/tabla';
import { HttpService } from 'src/app/servicios/http.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { BasesEscuelasAltaComponent } from './bases-escuelas-alta/bases-escuelas-alta.component';

@Component({
  selector: 'app-bases-escuelas',
  templateUrl: './bases-escuelas.component.html',
  styleUrls: ['./bases-escuelas.component.css']
})
export class BasesEscuelasComponent extends Tabla implements OnInit {

  constructor(HTTP: HttpService, private Ventana: VentanaService, private Alertas: AlertasService) {
    super(HTTP);
   }

  ngOnInit() {
    this.ConfigurarTabla({
      Tabla: {
        Nombre: "escuelas",
        Campos: [
          {Id: 'escuela',     Nombre: 'Sede', Ocultar: false,    Campo: 'escuelas.escuela',     Total: false},
          {Id: 'descripcion',     Nombre: 'Descripción', Ocultar: false,    Campo: 'escuelas.descripcion',     Total: false},
          {Id: 'direccion',     Nombre: 'Dirección', Ocultar: false,    Campo: 'escuelas.direccion',     Total: false},
        ],  
        Acciones: [
          {
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          },
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          },
          {Nombre: 'Ver foto', Tipo: 'interna', Accion: 'Foto'}
        ]
      }
    });
  }

  Agregar() {
    this.Ventana.Abrir(BasesEscuelasAltaComponent, {Ancho: '40%', Datos: null}).subscribe((respuesta) => {
      if(respuesta == 1) {
        this.Tabla();
      }
      else if(respuesta == 2) {
        this.Tabla();
        this.Agregar();
      }
    })
  }

  Accion(arg) {
    console.log(arg);
    if(arg.Accion == "Eliminar") {
      this.HTTP.post("escuela/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Escuela eliminada", "Cerrar");
          this.Tabla();
        }
      })
    }
    else if(arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(BasesEscuelasAltaComponent, {Ancho: '40%', Datos: arg.Datos}).subscribe(
        respuesta => {
          console.log(respuesta);
          if(respuesta == 1)
            this.Tabla();
        }
      );
    }
    else if(arg.Accion === "Foto") {
      window.open("http://agendas.estudiantesembajadores.com/agenda/public/archivos/escuelas/"+arg.Datos.id_principal+"."+arg.Datos.foto_ext);
    }
  }

}
