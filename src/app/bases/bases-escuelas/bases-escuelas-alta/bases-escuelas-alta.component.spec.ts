import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasesEscuelasAltaComponent } from './bases-escuelas-alta.component';

describe('BasesEscuelasAltaComponent', () => {
  let component: BasesEscuelasAltaComponent;
  let fixture: ComponentFixture<BasesEscuelasAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasesEscuelasAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasesEscuelasAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
