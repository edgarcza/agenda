import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-bases-escuelas-alta',
  templateUrl: './bases-escuelas-alta.component.html',
  styleUrls: ['./bases-escuelas-alta.component.css']
})
export class BasesEscuelasAltaComponent implements OnInit {

  BCAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Nombre', 'escuela', 'text', [Validators.required], 12, [{}]),
    new Formulario().Campo('Descripción', 'descripcion', 'textarea', [], 12, [{}]),
    new Formulario().Campo('Dirección', 'direccion', 'textarea', [Validators.required], 12, [{}]),
    new Formulario().Campo('Foto', 'foto', 'upload', [], 12, [{}]),
    new Formulario().Campo('Foto', 'foto_nombre', null, [], 12, [{}]),
  ];
  public BCAForm = new FormGroup({});

  constructor(
    public DRef: MatDialogRef<BasesEscuelasAltaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any,
    private Http: HttpService,
    private Alertas: AlertasService
  ) { }

  ngOnInit() {
    
  }

  Cerrar(caso = 0) {
    this.DRef.close(caso);
  }

  Submit(caso) {
    let {value} = this.BCAForm;
    let Escuela: any = {};
    Escuela.id = value.id; 
    Escuela.escuela = value.escuela;
    Escuela.descripcion = value.descripcion;
    Escuela.direccion = value.direccion;
    this.Http.post("escuela/guardar/foto", {Datos: Escuela, Archivo: {Base64: value.foto, Nombre: value.foto_nombre}}).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Escuela guardada", "Cerrar");
        this.Cerrar(caso);
      }
    })
  }

}
