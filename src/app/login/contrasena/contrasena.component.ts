import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../modulos/formulario/formulario';
import { HttpService } from '../../servicios/http.service';
import { SesionService } from '../../servicios/sesion.service';
import { Router, ActivatedRoute} from '@angular/router';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-contrasena',
  templateUrl: './contrasena.component.html',
  styleUrls: ['./contrasena.component.css']
})
export class ContrasenaComponent implements OnInit {

  public LControles = [];
  public LForm = new FormGroup({});

  constructor(private HTTP: HttpService, private Sesion: SesionService, private Router: Router, private Alertas: AlertasService, private ARoute: ActivatedRoute) { }

  ngOnInit() {
    if(this.ARoute.snapshot.queryParams.c) {
      // console.log(this.ARoute.snapshot.params);
      this.LControles.push(new Formulario().Campo('Nueva contraseña', 'password', 'text', [Validators.required], 12));
      this.LControles.push(new Formulario().Campo('Confirmar contraseña ', 'password2', 'text', [Validators.required], 12));
    }
    else {
      this.LControles.push(new Formulario().Campo('Correo electrónico', 'email', 'text', [Validators.required, Validators.email], 12));
    }


  }

  Enviar(datos) {
    // console.log(this.LForm.value);
    if(this.ARoute.snapshot.queryParams.c) {
      let Datos: any = {};
      Datos.password = this.LForm.value.password;
      Datos.password2 = this.LForm.value.password2;
      Datos.cod = this.ARoute.snapshot.queryParams.c;
      this.HTTP.post("contrasena2", Datos).subscribe((respuesta: any) => {
        console.log(respuesta);
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Contraseña cambiada");
          this.Router.navigate(["/Iniciar"]);
        }
        else {
          if(respuesta.error == 1) {
            this.Alertas.MSB_Mostrar("Las contraseñas no son iguales");
          }
          else {
            this.Alertas.MSB_Mostrar("Error al cambiar la contraseña");
          }
        }
      });
    }
    else {
      this.HTTP.post("contrasena1", this.LForm.value.email).subscribe((respuesta: any) => {
        console.log(respuesta);
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Correo enviado");
        }
      });
    }
  }

}
