import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../modulos/formulario/formulario';
import { HttpService } from '../../servicios/http.service';
import { SesionService } from '../../servicios/sesion.service';
import { Router} from '@angular/router';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  LControles = [
    new Formulario().Campo('Nombres', 'nombres', 'text', [Validators.required], 12, [{}]),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 12, [{}]),
    new Formulario().Campo('Correo electrónico', 'email', 'text', [Validators.required, Validators.email], 12),
    new Formulario().Campo('Contraseña', 'password', 'password', [Validators.required], 12),
    new Formulario().Campo('Confirmar contraseña', 'password2', 'password', [Validators.required], 12),
  ];
  public LForm = new FormGroup({});

  constructor(private HTTP: HttpService, private Sesion: SesionService, private Router: Router, private Alertas: AlertasService) { }

  ngOnInit() {
  }

  Enviar(datos) {
    if(this.LForm.value.password !== this.LForm.value.password2) {
      return this.Alertas.MSB_Mostrar("Las contraseñas no son iguales");
    }
    console.log(this.LForm.value);
    this.HTTP.post("registro", this.LForm.value).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Usuario registrado");
        this.Router.navigate(['/Iniciar']);
      }
      else {
        if(respuesta.error == 1) 
          this.Alertas.MSB_Mostrar("El usuario ya existe");
      }
    });
    // this.HTTP.post("login", this.LForm.value).subscribe((respuesta) => {
    //   console.log(respuesta);
    // });
  }

}
