import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../modulos/formulario/formulario';
import { HttpService } from '../servicios/http.service';
import { SesionService } from '../servicios/sesion.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  LControles = [
    new Formulario().Campo('Correo electrónico', 'email', 'text', [Validators.required, Validators.email], 12),
    new Formulario().Campo('Contraseña', 'password', 'password', [Validators.required], 12),
  ];
  public LForm = new FormGroup({});

  constructor(private HTTP: HttpService, private Sesion: SesionService, private Router: Router) { }

  ngOnInit() {
  }

  Enviar(datos) {
    console.log(this.LForm.value);
    this.HTTP.post("login", this.LForm.value).subscribe((respuesta: {usuario?}) => {
      console.log(respuesta);

      localStorage.setItem("ss", respuesta.usuario.remember_token);
      this.Sesion.Verificar();
      this.Router.navigate(['/Inicio/Inscribir']);
    });
    // this.HTTP.post("login", this.LForm.value).subscribe((respuesta) => {
    //   console.log(respuesta);
    // });
  }

  http() {
    // this.HTTP.get("get").subscribe((respuesta) => {
    //   console.log(respuesta)
    // })
    // this.HTTP.post("post").subscribe((respuesta) => {
    //   console.log(respuesta)
    // })
    // this.HTTP.post("token").subscribe((respuesta) => {
    //   console.log(respuesta)
    // })
    // this.HTTP.get("token").subscribe((respuesta) => {
    //   console.log(respuesta)
    // })
    this.Sesion.Verificar();
  }
}
