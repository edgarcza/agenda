import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { InicioComponent } from './inicio/inicio.component';
import { TablaComponent } from './modulos/tabla/tabla.component';
import { FormularioComponent } from './modulos/formulario/formulario.component';
import { AddComponent } from './modulos/add/add.component';
import { DialogoComponent } from './modulos/dialogo/dialogo.component';
import { RegistroComponent } from './login/registro/registro.component';
import { BasesEscuelasComponent } from './bases/bases-escuelas/bases-escuelas.component';
import { BasesEscuelasAltaComponent } from './bases/bases-escuelas/bases-escuelas-alta/bases-escuelas-alta.component';
import { BasesMateriasComponent } from './bases/bases-materias/bases-materias.component';
import { BasesMateriasAltaComponent } from './bases/bases-materias/bases-materias-alta/bases-materias-alta.component';
import { BasesHorariosComponent } from './bases/bases-horarios/bases-horarios.component';
import { BasesHorariosAltaComponent } from './bases/bases-horarios/bases-horarios-alta/bases-horarios-alta.component';
import { BasesGruposComponent } from './bases/bases-grupos/bases-grupos.component';
import { BasesGruposAltaComponent } from './bases/bases-grupos/bases-grupos-alta/bases-grupos-alta.component';
import { InscripcionComponent } from './inscripcion/inscripcion.component';
import { BasesIncripcionesComponent } from './bases/bases-incripciones/bases-incripciones.component';
import { ColumnasComponent } from './modulos/tabla/columnas.component';
import { BasesComponent } from './bases/bases.component';
import { UploadComponent } from './modulos/upload/upload.component';
import { BasesUsuariosComponent } from './bases/bases-usuarios/bases-usuarios.component';
import { BasesUsuariosAltaComponent } from './bases/bases-usuarios/bases-usuarios-alta/bases-usuarios-alta.component';
import { BasesInscripcionesAltaComponent } from './bases/bases-incripciones/bases-inscripciones-alta/bases-inscripciones-alta.component';
import { ContrasenaComponent } from './login/contrasena/contrasena.component';
import { RevisarComponent } from './revisar/revisar.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    FormularioComponent,
    AddComponent,
    DialogoComponent,
    TablaComponent,
    ColumnasComponent,
    UploadComponent,
    LoginComponent,
    InicioComponent,
    RegistroComponent,
    BasesEscuelasComponent,
    BasesEscuelasAltaComponent,
    BasesMateriasComponent,
    BasesMateriasAltaComponent,
    BasesHorariosComponent,
    BasesHorariosAltaComponent,
    BasesGruposComponent,
    BasesGruposAltaComponent,
    InscripcionComponent,
    BasesIncripcionesComponent,
    BasesComponent,
    BasesUsuariosComponent,
    BasesUsuariosAltaComponent,
    BasesInscripcionesAltaComponent,
    ContrasenaComponent,
    RevisarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ColumnasComponent,
    BasesEscuelasAltaComponent,
    BasesMateriasAltaComponent,
    BasesHorariosAltaComponent,
    BasesGruposAltaComponent,
    BasesUsuariosAltaComponent,
    BasesInscripcionesAltaComponent
  ]
})
export class AppModule { }
