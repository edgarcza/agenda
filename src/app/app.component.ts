import { Component } from '@angular/core';
import { HttpService } from './servicios/http.service';
import { SesionService } from './servicios/sesion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Token;
  
  constructor(private HTTP: HttpService, private Sesion: SesionService) { }

  ngOnInit(): void {
    this.Sesion.Verificar();
  }
}
