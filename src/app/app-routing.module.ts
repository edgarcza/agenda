import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { InicioComponent } from './inicio/inicio.component';
import { RegistroComponent } from './login/registro/registro.component';
import { InscripcionComponent } from './inscripcion/inscripcion.component';
import { AdminComponent } from './admin/admin.component';
import { BasesEscuelasComponent } from './bases/bases-escuelas/bases-escuelas.component';
import { BasesMateriasComponent } from './bases/bases-materias/bases-materias.component';
import { BasesHorariosComponent } from './bases/bases-horarios/bases-horarios.component';
import { BasesGruposComponent } from './bases/bases-grupos/bases-grupos.component';
import { BasesIncripcionesComponent } from './bases/bases-incripciones/bases-incripciones.component';
import { BasesUsuariosComponent } from './bases/bases-usuarios/bases-usuarios.component';
import { ContrasenaComponent } from './login/contrasena/contrasena.component';

const routes: Routes = [
  { 
    // path: '', redirectTo: '/Inicio', pathMatch: 'full' 
    path: '', redirectTo: '/Iniciar', pathMatch: 'full' 
  },
  { 
    path: 'Iniciar', component: LoginComponent
  },
  { 
    path: 'Inicio', component: InicioComponent, children: [
      {path: 'Inscribir', component: InscripcionComponent},
      {path: 'Admin', component: AdminComponent, children: [
        {path: 'Escuelas', component: BasesEscuelasComponent},
        {path: 'Materias', component: BasesMateriasComponent},
        {path: 'Horarios', component: BasesHorariosComponent},
        {path: 'Grupos', component: BasesGruposComponent},
        {path: 'Inscripciones', component: BasesIncripcionesComponent},
        {path: 'Usuarios', component: BasesUsuariosComponent},
      ]},
    ]
  },
  { 
    path: 'Registro', component: RegistroComponent
  },
  { 
    path: 'Contrasena', component: ContrasenaComponent
  },
  { 
    path: 'Contrasena/:cod', component: ContrasenaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
