import { Component, OnInit } from '@angular/core';
import { SesionService } from '../servicios/sesion.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  public SideNav = false;
  public Menu: any = [
    {Titulo: 'Administrar', Abierto: false, Icono: 'build', Link: 'Admin', Elementos: [], Admin: true},
    {Titulo: 'Inscribir', Abierto: false, Icono: 'assignment_ind', Link: 'Inscribir', Elementos: [], Admin: false},
    // {Titulo: 'Cursos', Abierto: false, Icono: 'book', Link: 'Bases/Cursos', Elementos: []},
    // {Titulo: 'Administración', Abierto: false, Icono: 'work', Link: '', Elementos: [
    //   {Titulo: 'Pagos institución', Link: 'Pagos/Institucion', Icono: ''},
    //   {Titulo: 'Pagos Keystone', Link: 'Pagos/Keystone', Icono: ''},
    // ]},
  ]

  constructor(public Sesion: SesionService) { }

  ngOnInit() {
  }

  CerrarSesion() {
    localStorage.removeItem("ss");
    this.Sesion.Verificar();
  }

}
