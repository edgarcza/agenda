import { Component, OnInit } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {
  heroes = [
    'Windstorm',
    'Bombasto',
    'Magneta',
    'Tornado'
  ];
  constructor() { }

  ngOnInit() {
  }

}
