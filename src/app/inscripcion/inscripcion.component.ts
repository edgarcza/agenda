import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../modulos/formulario/formulario';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { SesionService } from '../servicios/sesion.service';
import { Tabla } from 'src/app/modulos/tabla/tabla';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from '../modulos/formulario/FechaAdaptador';

@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  styleUrls: ['./inscripcion.component.css'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'mx'},
  ]
})
export class InscripcionComponent extends Tabla implements OnInit {

  BCAControles = [
    new Formulario().Campo('ID', 'id', '', null, 12),
    new Formulario().Campo('Sede', 'id_escuela', 'select', [Validators.required], 12, [{}]),
    new Formulario().Campo('Curso', 'id_materia', 'select', [Validators.required], 12, [{}]),
    // new Formulario().Campo('Dia', 'dia', 'date', [Validators.required], 12, null, new Date()),
    new Formulario().Campo('Horario', 'id_horario', 'select', [Validators.required], 12, [{}]),
    new Formulario().Campo('Grupo', 'id_grupo', 'select', [Validators.required], 12, [{}]),
  ];
  public BCAForm = new FormGroup({});

  public BQ_Usuario = "";
  public BQ_UsuarioSelect = null;
  public BQ_Usuarios = [];

  public InscripcionDia = new Date();
  public Fechas = {
    min: new Date(),
    max: new Date(2019, 11, 16),
    bloqs: (date: Date) => {console.log(date.getDay()); return (date.getDay()) !== 0}
  }

  public EscuelaSeleccionada = {
    id: 0,
    foto_ext: 'jpg'
  };

  constructor(
    private Http: HttpService,
    private Alertas: AlertasService,
    public Sesion: SesionService
  ) {
    super(Http)
  }

  ngOnInit() {
    this.ConfigurarTabla({
      Tabla: {
        Nombre: "inscripciones",
        Campos: [
          // {Id: 'email',     Nombre: 'Usuario', Ocultar: false,    Campo: 'materias.materia',     Total: false},
          // {Id: 'nombres',     Nombre: 'Nombres', Ocultar: false,    Campo: 'usuarios.nombres',     Total: false},
          // {Id: 'apellidos',     Nombre: 'Apellidos', Ocultar: false,    Campo: 'usuarios.apellidos',     Total: false},
          { Id: 'escuela', Nombre: 'Sede', Ocultar: false, Campo: 'escuelas.escuela', Total: false },
          { Id: 'materia', Nombre: 'Curso', Ocultar: false, Campo: 'materias.materia', Total: false },
          { Id: 'horario', Nombre: 'Hora', Ocultar: false, Campo: 'horarios.horario', Total: false },
          { Id: 'dia', Nombre: 'Fecha', Ocultar: false, Campo: 'inscripciones.dia', Total: false },
          // {Id: 'hh',     Nombre: 'Hora', Ocultar: false,    Campo: 'horarios.hh',     Total: false},
          // {Id: 'mm',     Nombre: 'Minuto', Ocultar: false,    Campo: 'horarios.mm',     Total: false},
          { Id: 'gdescripcion', Nombre: 'Descripción de grupo', Ocultar: false, Campo: 'grupos.gdescripcion', Total: false },
          { Id: 'fecha_examen', Nombre: 'Fecha del examen', Ocultar: false, Campo: 'grupos.fecha_examen ', Total: false },
          { Id: 'calificacion', Nombre: 'Calificación', Ocultar: false, Campo: 'inscripciones.calificacion', Total: false },
        ]
      }
    }, [
      { Campo: 'inscripciones.id_usuario', Valor: this.Sesion.Usuario.id, Condicion: '=' }
    ]);

    this.Sesion.Verificado.subscribe((res) => {
      this.ConfigurarTabla(null, [
        { Campo: 'inscripciones.id_usuario', Valor: this.Sesion.Usuario.id, Condicion: '=' }
      ]);
    })

    this.Http.get("escuelas").subscribe((res: any) => {
      new Formulario().CampoSelect(this.BCAControles, "id_escuela", res.datos, "id", "escuela");
    });
    this.Http.get("materias").subscribe((res: any) => {
      new Formulario().CampoSelect(this.BCAControles, "id_materia", res.datos, "id", "materia");
    });
    this.Http.get("horarios").subscribe((res: any) => {
      new Formulario().CampoSelect(this.BCAControles, "id_horario", res.datos, "id", "horario");
    });

    this.Http.get("grupos").subscribe((res: any) => {

      // CampoSelect(controles, campo, opciones = [], valor, nombre) {
      // var OPFormato = res.datos.map(function(elm) {
      //     return { Valor: elm['id_grupo'], Nombre: elm['']};
      // });

      // this.CampoMod(controles, campo, {Opciones: OPFormato});

      // new Formulario().CampoSelect(this.BCAControles, "id_escuela", res.datos, "id", "escuela");

    });

  }

  BQ() {
    console.log(this.BQ_Usuario);
    this.Http.post("usuarios/buscar", { Filtro: this.BQ_Usuario }).subscribe((res: any) => {
      console.log(res);
      this.BQ_Usuarios = res.datos;
    })
  }

  Construido(a) {
    this.BCAForm.get("id_escuela").valueChanges.subscribe((val) => {
      this.Http.post("grupos", {
        Filtros: [
          { Campo: 'id_escuela', Valor: val, Condicion: '=' }
        ], Paginador: null
      }).subscribe((res: any) => {
        new Formulario().CampoSelect(this.BCAControles, "id_grupo", res.datos, "id_principal", "gdescripcion");

        this.Http.post("escuelas", {
          Filtros: [
            { Campo: 'id', Valor: val, Condicion: '=' }
          ], Paginador: null
        }).subscribe((res2: any) => {
          console.log(res2);
          this.EscuelaSeleccionada.id = val;
          this.EscuelaSeleccionada.foto_ext = res2.datos[0].foto_ext;
        });

      });
    });

    this.BCAForm.get("id_materia").valueChanges.subscribe((val) => {
      this.Http.post("grupos", {
        Filtros: [
          { Campo: 'id_materia', Valor: val, Condicion: '=' }
        ], Paginador: null
      }).subscribe((res: any) => {
        new Formulario().CampoSelect(this.BCAControles, "id_grupo", res.datos, "id_principal", "gdescripcion");
      });
    });

    this.BCAForm.get("id_horario").valueChanges.subscribe((val) => {
      this.Http.post("grupos", {
        Filtros: [
          { Campo: 'id_horario', Valor: val, Condicion: '=' }
        ], Paginador: null
      }).subscribe((res: any) => {
        new Formulario().CampoSelect(this.BCAControles, "id_grupo", res.datos, "id_principal", "gdescripcion");
      });
    });
  }

  Submit(caso) {
    // console.log
    let UsuarioID = 0;
    if (this.Sesion.Usuario.tipo == 1) {
      UsuarioID = this.BQ_UsuarioSelect;
    }
    else {
      UsuarioID = this.Sesion.Usuario.id;
    }
    this.Http.post("inscripcion/inscribirse", {
      id_grupo: this.BCAForm.get('id_grupo').value,
      id_usuario: UsuarioID,
      // dia: this.BCAForm.get('dia').value
      dia: this.InscripcionDia
    }).subscribe((respuesta: any) => {
      console.log(respuesta);
      if (respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Inscrito", "Cerrar");
        window.open('https://agenda.estudiantesembajadores.com/agenda/public/pdf/' + respuesta.datos.id + '.pdf', '_blank');
        this.Tabla();
      }
      else {
        if (respuesta.error == 1) {
          this.Alertas.MSB_Mostrar("El usuario ya está inscrito en el grupo");
        }
        else if (respuesta.error == 2) {
          this.Alertas.MSB_Mostrar("El grupo ya está lleno en el día indicado");
        }
      }
    })
  }

  Accion(arg) {

  }

}
