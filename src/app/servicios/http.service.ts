import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
// import { HttpResponse } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  
  // private Headers = new HttpHeaders({'Content-Type': 'application/json'});
	// private Headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');	
	private Headers = new HttpHeaders().set('Content-Type','application/json');	
  public Url = "https://agenda.estudiantesembajadores.com/agenda/public/";
  private Token = "";

  constructor(
    public Http: HttpClient
    ) {
      // this.Headers = this.Headers.set('Authorization', 'Bearer ' +  this.Token);
  }

  token(token) {
    this.Token = token;
    // this.Headers = this.Headers.set('X-TOKEN', token);
    // this.Headers = this.Headers.set('X-CSRF-TOKEN', token);
    // this.Headers = this.Headers.set('X-XSRF-TOKEN', token);
    // this.Headers = this.Headers.set('CSRF-TOKEN', token);
    // this.Headers = this.Headers.set('CSRF-TOKEN', token);
  }

  getAll(url): Observable<HttpResponse<any>> {
    console.log("[GET All] " + url)
    return this.Http.get<any>(
      this.Url + url, { observe: 'response', headers: this.Headers });
  }

  get(url) {
    console.log("[GET] " + url)
    return this.Http.get(this.Url + url, {headers: this.Headers});
  }

  getUrl(url, headers = this.Headers) {
    console.log("[GET] " + url)
    return this.Http.get(url, {headers: headers});
  }

  post(url, datos, headers = this.Headers) {
    console.log("[POST] " + url)
    // console.log("[TOKEN] " + this.Token)
    // console.log("[HEADERS] ", headers)
    return this.Http.post(this.Url + url, {
      // "_token": "{{ csrf_token() }}",
      // _token: this.Token,
      datos: datos,  
      // data: {
      // }
    }, {headers: headers});
  }

}
