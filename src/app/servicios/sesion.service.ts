import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.service';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  public Auth: boolean = false;
  public Usuario: any = {};
  public Verificado: EventEmitter<boolean> = new EventEmitter();

  constructor(private HTTP: HttpService, private Router: Router, private ARoute: ActivatedRoute) { }

  Verificar() {    
    this.HTTP.post("sesion", {remember_token: localStorage.getItem("ss")}).subscribe((respuesta: any) => {
      console.log(respuesta);
      // console.log(this.Router.url);
      /**Validar sesion y hacer variable Auth true, con eso validar las vistas */
      if(respuesta.proceso) {
        this.Auth = true;
        this.Usuario = respuesta.usuario;
        this.Verificado.next(true);
      }
      else {
        if(!this.Router.url.includes("/Contrasena"))
          this.Router.navigate(['/Iniciar']);
      }
    });
  }

}
