-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-08-2019 a las 11:09:47
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tesolmex_agendas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuelas`
--

CREATE TABLE `escuelas` (
  `id` int(11) NOT NULL,
  `escuela` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `foto_ext` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `escuelas`
--

INSERT INTO `escuelas` (`id`, `escuela`, `descripcion`, `direccion`, `foto_ext`, `created_at`, `updated_at`) VALUES
(1, 'Sede 1', 'Sede 1', '.', 'jpg', '2019-08-27 15:26:24', '2019-08-28 15:54:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `id_escuela` int(11) NOT NULL,
  `id_horario` int(11) NOT NULL,
  `fecha_examen` date NOT NULL,
  `gdescripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `id_materia`, `id_escuela`, `id_horario`, `fecha_examen`, `gdescripcion`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2019-08-31', 'Grupo 1', '2019-08-27 00:00:00', '2019-08-27 00:00:00'),
(2, 1, 1, 2, '2019-08-31', 'Grupo 1', '2019-08-27 00:00:00', '2019-08-27 00:00:00'),
(3, 1, 1, 3, '2019-08-31', 'Grupo 1', '2019-08-27 00:00:00', '2019-08-27 00:00:00'),
(4, 1, 1, 4, '2019-08-31', 'Grupo 1', '2019-08-27 00:00:00', '2019-08-27 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL,
  `hh` int(11) NOT NULL,
  `mm` int(11) NOT NULL,
  `horario` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`id`, `hh`, `mm`, `horario`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 10, 0, '10:00', 'M1', '2019-08-27 15:47:38', '2019-08-27 15:47:38'),
(2, 12, 0, '12:00', 'V1', '2019-08-27 15:47:45', '2019-08-27 15:47:45'),
(3, 14, 0, '14:00', 'V2', '2019-08-27 15:47:57', '2019-08-27 15:47:57'),
(4, 15, 0, '15:00', 'V3', '2019-08-27 15:48:07', '2019-08-27 15:48:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripciones`
--

CREATE TABLE `inscripciones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `calificacion` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `inscripciones`
--

INSERT INTO `inscripciones` (`id`, `id_usuario`, `id_grupo`, `calificacion`, `created_at`, `updated_at`) VALUES
(18, 11, 11, 0, '2019-08-23 19:43:02', '2019-08-23 19:43:02'),
(20, 13, 11, 0, '2019-08-23 21:47:44', '2019-08-23 21:47:44'),
(21, 16, 12, 0, '2019-08-26 15:43:03', '2019-08-26 15:43:03'),
(22, 1, 11, 0, '2019-08-26 16:38:46', '2019-08-26 16:38:46'),
(23, 1, 16, 0, '2019-08-26 16:45:11', '2019-08-26 16:45:11'),
(24, 15, 11, 0, '2019-08-26 17:10:27', '2019-08-26 17:10:27'),
(29, 18, 1, 0, '2019-08-27 15:55:59', '2019-08-27 15:55:59'),
(31, 12, 1, 0, '2019-08-28 16:03:15', '2019-08-28 16:03:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `id` int(11) NOT NULL,
  `materia` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `materia`, `created_at`, `updated_at`) VALUES
(1, 'Materia 1', '2019-08-10 15:54:13', '2019-08-19 22:15:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL DEFAULT 2,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `password`, `nombres`, `apellidos`, `remember_token`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'ecantu@estudiantesembajadores.com', '$2y$10$2/BPimDAOkfo6.ZOLx8UvuOeBtKErGEy/zE1olvw.ha3arVhLG5Su', '', '', 'a23z5it337VFDQAXBSDhMPl4QgypoPqRKPpnQ7FN0fEDVxqKdR5RtqRzbzt1', 1, '2019-08-10 15:26:55', '2019-08-10 15:26:55'),
(3, 'ecantuds3@estudiantesembajadores.com', '$2y$10$Ztn5exYZuUUueLlt.nXD0.ZMwpM6vF3FQtt/Row7Owwuw2TOKXrWC', '', '', 'IpkjcA2y64P28YbaW7MQ6pNXusf6NEHbTpGbEfwbaPX7pnrpA097wExgc52B', 0, '2019-08-10 15:31:40', '2019-08-10 15:31:40'),
(12, 'edgarcantu.za@gmail.com', '$2y$10$ww10Lo9ENTwBBMkNOaDdM.EpaYIBbe8RwItAKoqFO4AyQFyEYm/Lm', 'Edgar', 'Cantú', 'aWQQ4B4UPimresTE2BFWp4YO7jE7I75bF7pLWEXjRSbEkHQQ39jmXmdCEXkf', 2, '2019-08-23 19:50:02', '2019-08-23 19:50:02'),
(14, 'victor@estudiantesembajadores.com', '$2y$10$EfEGnTCvZFD0O3f6RmZdGOtz7FoaAC.MSMcJLrU3s9IgTG0Zrd77q', 'Juan Víctor', 'Amador Acevedo', 'wpvG0x6G2mk8gyGjZYpqHfXgSH5AyigtDGDFL9zSX6yP5WZ9sEm1ZCQxNWNN', 2, '2019-08-26 15:14:50', '2019-08-26 15:14:50'),
(16, 'wherrera@estudiantesembajadores.com', '$2y$10$80yzdiKjOrLBDzC3nAT7i.tXcWRZhF5DOaulrJ.rzY0fWZdYkxmG6', 'William', 'Herrera', 'ILEyACQTM8buOQUaGLvCGR10JgogVzvNVre0FLUYMebFKgD3U6e2GSDyT4Ua', 2, '2019-08-26 15:42:20', '2019-08-26 15:42:20'),
(18, 'eder_josimar@hotmail.com', '$2y$10$LKBYIIBBDBBhABVGVJX.SetxExcxCLg8Qxce4G/SHo6FIi7YxrIj2', 'Eder', 'Jiménez', 'cCO4MacJt9efRAhQshLgnfCrdql2luvPz16EQQuw8bQ1WG8z0edXl5PkFtku', 2, '2019-08-27 15:38:53', '2019-08-27 15:38:53');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `escuelas`
--
ALTER TABLE `escuelas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `escuelas`
--
ALTER TABLE `escuelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `materias`
--
ALTER TABLE `materias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
